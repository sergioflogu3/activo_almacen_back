<?php

namespace App\Http\Controllers;

use App\Models\Activo;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Random\RandomException;


class ActivoController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $activos = Activo::all();
        return response()->json($activos);
    }

    /**
     * @return JsonResponse
     * @throws RandomException
     */
    public function store(): JsonResponse
    {
        $this->getArr();
        $activo = Activo::create([
            'id_proyecto' => request()->id_proyecto,
            'id_categoria' => request()->id_categoria,
            'nombre_activo' => request()->nombre_activo,
            'descripcion' => request()->descripcion,
            'fecha_adquisicion' => request()->fecha_adquisicion,
            'valor_adquisicion' => request()->valor_adquisicion,
            'codigo_literal' => $this->generaCodigo(true),
            'codigo_numeral' => $this->generaCodigo(false),
            'usuario_registro' => 'sergio.flores'
        ]);

        return response()->json($activo, 201);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function update(int $id): JsonResponse
    {
        $this->getArr();
        $activo = Activo::findOrFail($id);
        $activo->update([
            'id_proyecto' => request()->id_proyecto,
            'id_categoria' => request()->id_categoria,
            'nombre_activo' => request()->nombre_activo,
            'descripcion' => request()->descripcion,
            'fecha_adquisicion' => request()->fecha_adquisicion,
            'valor_adquisicion' => request()->valor_adquisicion,
        ]);
        return response()->json($activo);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $activo = Activo::findOrFail($id);
        $activo->delete();
        return response()->json($activo);
    }

    /**
     * @param bool $codigo
     * @return string
     * @throws RandomException
     */
    private function generaCodigo(bool $codigo): string
    {
        if($codigo) $caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        else $caracteres = '0123456789';
        $codigo = '';
        $max = strlen($caracteres) - 1;

        for ($i = 0; $i < 10; $i++) {
            $codigo .= $caracteres[random_int(0, $max)];
        }

        return $codigo;
    }

    /**
     * @return void
     */
    public function getArr(): void
    {
        request()->validate([
            'id_proyecto' => 'required|numeric',
            'id_categoria' => 'required|numeric',
            'nombre_activo' => 'required',
            'descripcion' => 'required',
            'fecha_adquisicion' => 'required',
            'valor_adquisicion' => 'required',
        ]);
    }


}
