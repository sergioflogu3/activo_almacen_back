<?php

namespace App\Http\Controllers;

use App\Models\Asignacion;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class AsignacionController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $asignaciones = Asignacion::all();
        return response()->json($asignaciones);
    }

    /**
     * @return JsonResponse
     */
    public function store(): JsonResponse
    {
        $this->getArr();
        $asignacion = Asignacion::create([
            'id_proyecto' => request()->id_proyecto,
            'id_personal' => request()->id_personal,
            'id_activo' => request()->id_activo,
            'id_ubicacion' => request()->id_ubicacion,
            'fecha_asignacion' => request()->fecha_asignacion,
            'usuario_registro' => 'sergio.flores',
        ]);
        return response()->json($asignacion, 201);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function update(int $id): JsonResponse
    {
        $this->getArr();
        $asignacion = Asignacion::findOrFail($id);
        $asignacion->update([
            'id_proyecto' => request()->id_proyecto,
            'id_personal' => request()->id_personal,
            'id_activo' => request()->id_activo,
            'id_ubicacion' => request()->id_ubicacion,
            'fecha_asignacion' => request()->fecha_asignacion,
            'usuario_registro' => 'sergio.flores'
        ]);
        return response()->json($asignacion);
    }

    /**
     * @param int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        $asignacion = Asignacion::findOrFail($id);
        $asignacion->delete();
        return response()->json($asignacion);
    }

    /**
     * @return void
     */
    public function getArr(): void
    {
        request()->validate([
            'id_proyecto' => 'required|numeric',
            'id_personal' => 'required|numeric',
            'id_activo' => 'required|numeric',
            'id_ubicacion' => 'required|numeric',
            'fecha_asignacion' => 'required'
        ]);
    }
}
