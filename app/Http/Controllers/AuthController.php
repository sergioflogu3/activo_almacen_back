<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    /**
     * @return JsonResponse
     */
    public function login(): JsonResponse
    {
        request()->validate([
            'username' => 'required',
            'password' => 'required'
        ]);
        $user = User::where('username', request()->username)
            ->first();
        if (!$user || !Hash::check(request()->password, $user->password)){
            return response()->json([
                'mensaje' => 'Credenciales incorrectas'
            ]);
        }
        $token = $user->createToken('auth_token')->plainTextToken;
        return response()->json([
            'mensaje' => 'Credenciales correctas',
            'token' => $token
        ]);
    }

    /**
     * @return JsonResponse
     */
    public function logout(): JsonResponse
    {
        $user = auth()->user();
        $user->tokens()->delete();
        return response()->json([
            'mensaje' => 'Credenciales eliminadas con exito'
        ]);
    }
}
