<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Activo extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'activos';
    protected $fillable = [
        'id_proyecto', 'id_categoria', 'nombre_activo', 'descripcion', 'fecha_adquisicion',
        'valor_adquisicion', 'codigo_literal', 'codigo_numeral', 'usuario_registro', 'imagen'
    ];
}
