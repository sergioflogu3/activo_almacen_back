<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Asignacion extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'asignaciones';
    protected $fillable = [
        'id_proyecto', 'id_personal', 'id_activo', 'id_ubicacion', 'fecha_asignacion', 'usuario_registro'
    ];
}
