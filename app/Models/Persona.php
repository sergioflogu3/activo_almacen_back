<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
    use HasFactory;
    protected $table = 'personas';
    protected $fillable = [
        'tipo_documento', 'numero_documento', 'complemento', 'nombres', 'primer_apellido',
        'segundo_apellido', 'fecha_nacimiento'
    ];
}
