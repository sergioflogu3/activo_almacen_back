<?php

namespace Database\Seeders;

use App\Models\Agencia;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AgenciaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Agencia::create([
            'nombre_agencia' => 'UNICEF'
        ]);
        Agencia::create([
            'nombre_agencia' => 'ONU'
        ]);
    }
}
