<?php

namespace Database\Seeders;

use App\Models\Almacen;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class AlmacenSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Almacen::create([
            'nombre_almacen' => 'Almacen 1',
            'direccion' => 'Avenida siempre viva N° 331'
        ]);
        Almacen::create([
            'nombre_almacen' => 'Almacen 2',
            'direccion' => 'Avenida Las Americas N° 23'
        ]);
    }
}
