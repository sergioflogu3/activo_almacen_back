<?php

namespace Database\Seeders;

use App\Models\CategoriaActivo;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoriaActivoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CategoriaActivo::create([
            'nombre_categoria' => 'Edificios y Terrenos',
            'descripcion' => 'Activos inmuebles como edificios y terrenos, incluidas sus mejoras.'
        ]);
        CategoriaActivo::create([
            'nombre_categoria' => 'Maquinaria y Equipos',
            'descripcion' => 'Maquinaria y equipos utilizados en la producción y otras actividades industriales.'
        ]);
        CategoriaActivo::create([
            'nombre_categoria' => 'Mobiliario y Equipos de Oficina',
            'descripcion' => 'Mobiliario y equipos utilizados en oficinas.'
        ]);
        CategoriaActivo::create([
            'nombre_categoria' => 'Vehículos',
            'descripcion' => 'Vehículos utilizados para el transporte y otras actividades empresariales.'
        ]);
        CategoriaActivo::create([
            'nombre_categoria' => 'Equipos de Tecnología y Comunicación',
            'descripcion' => 'Equipos tecnológicos y de comunicación como computadoras y dispositivos de red.'
        ]);
        CategoriaActivo::create([
            'nombre_categoria' => 'Muebles y Enseres',
            'descripcion' => 'Muebles y enseres diversos utilizados en diferentes áreas de la empresa.'
        ]);
        CategoriaActivo::create([
            'nombre_categoria' => 'Equipos Médicos',
            'descripcion' => 'Equipos utilizados en entornos médicos y de atención sanitaria.'
        ]);
        CategoriaActivo::create([
            'nombre_categoria' => 'Herramientas y Equipos Pequeños',
            'descripcion' => 'Herramientas y equipos pequeños utilizados en diversas tareas.'
        ]);
        CategoriaActivo::create([
            'nombre_categoria' => 'Activos Intangibles',
            'descripcion' => 'Activos no físicos como patentes, marcas registradas y derechos de autor.'
        ]);
    }
}
