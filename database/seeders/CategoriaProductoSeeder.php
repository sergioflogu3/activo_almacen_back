<?php

namespace Database\Seeders;

use App\Models\CategoriaProducto;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class CategoriaProductoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        CategoriaProducto::create([
            'nombre_categoria' => 'Electrónica',
            'descripcion' => 'Dispositivos electrónicos y accesorios.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Ropa y Accesorios',
            'descripcion' => 'Ropa para hombres, mujeres y niños, además de accesorios.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Alimentos y Bebidas',
            'descripcion' => 'Productos alimenticios y bebidas de todo tipo.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Hogar y Jardín',
            'descripcion' => 'Muebles y artículos para el hogar y el jardín.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Salud y Belleza',
            'descripcion' => 'Productos de cuidado personal, cosméticos y equipos de ejercicio.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Automotriz',
            'descripcion' => 'Piezas, accesorios y productos de mantenimiento para automóviles.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Juguetes y Juegos',
            'descripcion' => 'Juguetes para niños, juegos de mesa y videojuegos.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Papelería y Oficina',
            'descripcion' => 'Suministros, equipos y muebles de oficina.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Deportes y Aire Libre',
            'descripcion' => 'Equipos deportivos, ropa y artículos para actividades al aire libre.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Mascotas',
            'descripcion' => 'Alimentos, accesorios y productos de cuidado para mascotas.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Libros y Medios',
            'descripcion' => 'Libros, música, películas y series.'
        ]);
        CategoriaProducto::create([
            'nombre_categoria' => 'Materiales de Construcción',
            'descripcion' => 'Materiales básicos, herramientas y accesorios de construcción.'
        ]);
    }
}
