<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\User;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();
        $this->call(AgenciaSeeder::class);
        $this->call(ProyectoSeeder::class);
        $this->call(CategoriaActivoSeeder::class);
        $this->call(CategoriaProductoSeeder::class);
        $this->call(PersonaSeeder::class);
        $this->call(PersonalSeeder::class);
         User::factory()->create([
             'username' => 'sergio.flores',
             'id_persona' => 1,
             'email' => 'sergio.flores@gmail.com',
         ]);
         $this->call(UbicacionSeeder::class);
    }
}
