<?php

namespace Database\Seeders;

use App\Models\Persona;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PersonaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Persona::create([
            'tipo_documento' => 'CI',
            'numero_documento' => '4900069',
            'complemento' => '',
            'nombres' => 'SERGIO ADRIAN',
            'primer_apellido' => 'FLORES',
            'segundo_apellido' => 'GUTIERREZ',
            'fecha_nacimiento' => '1983-03-05'
        ]);
        Persona::create([
            'tipo_documento' => 'CI',
            'numero_documento' => '4886323',
            'complemento' => '',
            'nombres' => 'CESAR',
            'primer_apellido' => 'ARUQUIPA',
            'segundo_apellido' => 'ARANA',
            'fecha_nacimiento' => '1983-11-15'
        ]);
        Persona::create([
            'tipo_documento' => 'CI',
            'numero_documento' => '4800068',
            'complemento' => '',
            'nombres' => 'MARCELO',
            'primer_apellido' => 'FLORES',
            'segundo_apellido' => 'GUTIERREZ',
            'fecha_nacimiento' => '1985-12-10'
        ]);
    }
}
