<?php

namespace Database\Seeders;

use App\Models\Proyecto;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProyectoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Proyecto::create([
            'id_agencia' => 1,
            'nombre_proyecto' => 'Proyecto X',
            'fecha_inicio' => now(),
            'fecha_fin' => '2025-12-31'
        ]);
        Proyecto::create([
            'id_agencia' => 1,
            'nombre_proyecto' => 'Proyecto Y',
            'fecha_inicio' => now(),
            'fecha_fin' => '2025-12-31'
        ]);
        Proyecto::create([
            'id_agencia' => 2,
            'nombre_proyecto' => 'Proyecto Z',
            'fecha_inicio' => now(),
            'fecha_fin' => '2025-12-31'
        ]);
    }
}
