<?php

use App\Http\Controllers\ActivoController;
use App\Http\Controllers\AsignacionController;
use App\Http\Controllers\AuthController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/
Route::post('/login', [AuthController::class,'login']);
Route::middleware('auth:sanctum')->group(function (){
    Route::post('/logout', [AuthController::class,'logout']);
    Route::get('/activo', [ActivoController::class, 'index']);
    Route::post('/activo', [ActivoController::class, 'store']);
    Route::put('/activo/{id}', [ActivoController::class, 'update']);
    Route::delete('/activo/{id}', [ActivoController::class, 'destroy']);

    Route::get('/asignacion', [AsignacionController::class, 'index']);
    Route::post('/asignacion', [AsignacionController::class, 'store']);
    Route::put('/asignacion/{id}', [AsignacionController::class, 'update']);
    Route::delete('/asignacion/{id}', [AsignacionController::class, 'destroy']);
});





